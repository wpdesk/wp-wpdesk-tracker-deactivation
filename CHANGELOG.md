## [2.0.2] - 2024-02-28
### Fixed
- permissions check in AJAX action

## [2.0.1] - 2024-02-28
### Added
- visual changes

## [2.0.0] - 2024-02-23
### Added
- new popup layout

## [1.5.2] - 2022-08-30
### Fixed
- de_DE translators

## [1.5.1] - 2022-08-29
### Fixed
- deactivation scripts

## [1.5.0] - 2022-08-16
### Added
- en_CA, en_GB translators

## [1.4.0] - 2022-08-16
### Added
- de_DE translators

## [1.3.0] - 2022-08-09
### Added
- en_AU translators

## [1.2.0] - 2022-06-20
### Added
- Ability to replace sender in filter wpdesk/tracker/sender/$plugin_slug

## [1.1.0] - 2022-04-12
### Changed
- allowed wpdesk/wp-wpdesk-tracker 3.0
- allowed wpdesk/wp-builder 2.0

## [1.0.8] - 2020-08-19
### Fixed
- changed deprecated jQuery .load() to .on('load', handler) 

## [1.0.7] - 2019-11-07
### Fixed
- elements sizing 

## [1.0.6] - 2019-11-06
### Added
- elements sizing 

## [1.0.5] - 2019-11-06
### Added
- modified english texts and polish translations

## [1.0.4] - 2019-11-04
### Added
- PopUp window auto sized and positioned

## [1.0.3] - 2019-11-04
### Fixed
- Translations

## [1.0.2] - 2019-11-04
### Fixed
- Translations

## [1.0.1] - 2019-10-28
### Added
- Translations

## [1.0.0] - 2019-10-22
### Added
- First version
