[![pipeline status](https://gitlab.com/wpdesk/wp-wpdesk-tracker-deactivation/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-wpdesk-tracker-deactivation/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-wpdesk-tracker-deactivation/badges/master/coverage.svg)](https://gitlab.com/wpdesk/wp-wpdesk-tracker-deactivation/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-wpdesk-tracker-deactivation/v/stable)](https://packagist.org/packages/wpdesk/wp-wpdesk-tracker-deactivation) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-wpdesk-tracker-deactivation/downloads)](https://packagist.org/packages/wpdesk/wp-wpdesk-tracker-deactivation) 
[![License](https://poser.pugx.org/wpdesk/wp-wpdesk-tracker-deactivation/license)](https://packagist.org/packages/wpdesk/wp-wpdesk-tracker-deactivation)

# Deactivation Tracker

A WordPress Library containing interfaces, abstracts and implementations to be used for plugin deactivations data tracking.

## Requirements

PHP 7.0 or later.

## Installation via Composer

In order to install the bindings via [Composer](http://getcomposer.org/) run the following command:

```bash
composer require wpdesk/wp-wpdesk-tracker-deactivation
```

## Example usage

### Creating deactivation tracker

The following code creates deactivation tracker. Once the site admin deactivates the plugin, a pop-up containing the deactivation reasons will appear. Right after the relevant deactivation reason is selected, the site admin's response will be sent to the tracker server.

```php
$deactivation_tracker = \WPDesk\Tracker\Deactivation\TrackerFactory::createDefaultTracker(
	'my-beautiful-plugin',
	'my-beautiful-plugin/my-beautiful-plugin.php',
	__( 'My Beautiful Plugin' )
);
$deactivation_tracker->hooks();
```

### Replacing the default sender

```php
class MySender implements WPDesk_Tracker_Sender {
	public function send_payload(array $payload){
	   // implement send_payload method.
	}
}

$plugin_slug = 'my-example-plugin';
add_filter( 'wpdesk/tracker/sender/' . $plugin_slug, 'replace_sender' );

function replace_sender() {
	return new MySender();
}
```
