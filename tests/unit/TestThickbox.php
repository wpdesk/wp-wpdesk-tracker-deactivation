<?php

use WPDesk\Tracker\Deactivation\DefaultReasonsFactory;
use WPDesk\Tracker\Deactivation\PluginData;
use WPDesk\Tracker\Deactivation\Thickbox;

class TestThickbox extends Content {

	protected $view = __DIR__ . '/../../src/WPDesk/Tracker/Deactivation/views/thickbox.php';

	protected function getContent() {
		$thickbox_id = 'tracker-tb-' . self::PLUGIN_SLUG;
		$plugin_title = self::PLUGIN_TITLE;
		$plugin_file = self::PLUGIN_FILE;
		$plugin_slug = self::PLUGIN_SLUG;
        $reasons = (new DefaultReasonsFactory())->createReasons();
		ob_start();
		include $this->view;
		return ob_get_clean();
	}

	/**
	 * Test getContent.
	 */
	public function testGetContent() {
        \WP_Mock::userFunction( 'checked', array(
            'return' => '',
        ) );
        \WP_Mock::userFunction( 'disabled', array(
            'return' => '',
        ) );
        \WP_Mock::passthruFunction( 'wp_kses_post' );
		$scripts = new Thickbox(new PluginData(self::PLUGIN_SLUG, self::PLUGIN_FILE, self::PLUGIN_TITLE), new DefaultReasonsFactory() );
		$this->assertEquals( $this->getContent(), $scripts->getContent() );
	}

}
