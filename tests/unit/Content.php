<?php

use WP_Mock\Tools\TestCase;
use WPDesk\Tracker\Deactivation\AjaxDeactivationDataHandler;
use WPDesk\Tracker\Deactivation\PluginData;
use WPDesk\Tracker\Deactivation\Scripts;

class Content extends TestCase {

	const PLUGIN_SLUG = 'slug';
	const PLUGIN_FILE = 'file';
	const PLUGIN_TITLE = 'title';

	protected $view = __DIR__ . '/../../src/WPDesk/Tracker/Deactivation/views/abstract.php';

	public function setUp(): void {
		\WP_Mock::setUp();
	}

	public function tearDown(): void {
		\WP_Mock::tearDown();
	}

	/**
	 * Get content.
	 *
	 * @return string
	 */
	protected function getContent() {

		\WP_Mock::userFunction( 'wp_create_nonce', array(
			'return' => 'nonce',
		) );

		\WP_Mock::userFunction( 'admin_url', array(
			'return' => 'admin_url',
		) );

		$plugin_title = self::PLUGIN_TITLE;
		$plugin_file = self::PLUGIN_FILE;
		$plugin_slug = self::PLUGIN_SLUG;
		$thickbox_id = 'tracker-tb-' . self::PLUGIN_SLUG;
		$ajax_action = AjaxDeactivationDataHandler::AJAX_ACTION . self::PLUGIN_SLUG;
		$ajax_nonce = wp_create_nonce( $ajax_action );
		ob_start();
		include $this->view;
		return ob_get_clean();
	}

}
