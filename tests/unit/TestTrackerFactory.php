<?php

use WP_Mock\Tools\TestCase;
use WPDesk\Tracker\Deactivation\AjaxDeactivationDataHandler;
use WPDesk\Tracker\Deactivation\DefaultReasonsFactory;
use WPDesk\Tracker\Deactivation\PluginData;
use WPDesk\Tracker\Deactivation\Scripts;
use WPDesk\Tracker\Deactivation\Thickbox;
use WPDesk\Tracker\Deactivation\Tracker;

class TestTrackerFactory extends TestCase {

	const PLUGIN_SLUG = 'slug';
	const PLUGIN_FILE = 'file';
	const PLUGIN_TITLE = 'title';

	public function setUp(): void {
		\WP_Mock::setUp();
	}

	public function tearDown(): void {
		\WP_Mock::tearDown();
	}

	/**
	 * Test createDefaultTracker.
	 */
	public function testCreateDefaultTracker() {
		$tracker = \WPDesk\Tracker\Deactivation\TrackerFactory::createDefaultTracker(self::PLUGIN_SLUG, self::PLUGIN_FILE, self::PLUGIN_TITLE);
		$this->assertInstanceOf( Tracker::class, $tracker);
	}

	/**
	 * Test createDefaultTrackerFromPluginData.
	 */
	public function testCreateDefaultTrackerFromPluginData() {
		$tracker = \WPDesk\Tracker\Deactivation\TrackerFactory::createDefaultTrackerFromPluginData(new PluginData(self::PLUGIN_SLUG, self::PLUGIN_FILE, self::PLUGIN_TITLE));
		$this->assertInstanceOf( Tracker::class, $tracker);
	}

	/**
	 * Test createCustomTracker.
	 */
	public function testCreateCustomTracker() {
		$sender = $this->createMock(\WPDesk_Tracker_Sender::class);
		$plugin_data = new PluginData(self::PLUGIN_SLUG, self::PLUGIN_FILE, self::PLUGIN_TITLE);
		$tracker = \WPDesk\Tracker\Deactivation\TrackerFactory::createCustomTracker(
			$plugin_data,
			new Scripts($plugin_data, new DefaultReasonsFactory()),
			new Thickbox($plugin_data, new DefaultReasonsFactory()),
			new AjaxDeactivationDataHandler($plugin_data, $sender)
		);
		$this->assertInstanceOf( Tracker::class, $tracker);
	}

	/**
	 * Test createCustomTracker.
	 */
	public function testCreateCustomTrackerEmptyParams() {
		$sender = $this->createMock(\WPDesk_Tracker_Sender::class);
		$tracker = \WPDesk\Tracker\Deactivation\TrackerFactory::createCustomTracker(
			new PluginData(self::PLUGIN_SLUG, self::PLUGIN_FILE, self::PLUGIN_TITLE),
			null,
			null,
			null
		);
		$this->assertInstanceOf(Tracker::class, $tracker);
	}

}
