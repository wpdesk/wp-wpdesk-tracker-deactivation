<?php

use WP_Mock\Tools\TestCase;
use WPDesk\Tracker\Deactivation\AjaxDeactivationDataHandler;
use WPDesk\Tracker\Deactivation\DefaultReasonsFactory;
use WPDesk\Tracker\Deactivation\PluginData;
use WPDesk\Tracker\Deactivation\Scripts;
use WPDesk\Tracker\Deactivation\Thickbox;
use WPDesk\Tracker\Deactivation\Tracker;
use WPDesk\Tracker\Deactivation\TrackerFactory;

class TestTracker extends TestCase {

	const PLUGIN_SLUG = 'slug';
	const PLUGIN_FILE = 'file';
	const PLUGIN_TITLE = 'title';

	public function setUp(): void {
		\WP_Mock::setUp();
	}

	public function tearDown(): void {
		\WP_Mock::tearDown();
	}

	/**
	 * Test hooks.
	 */
	public function testHooks() {
		$sender = $this->createMock(\WPDesk_Tracker_Sender::class);
		$plugin_data = new PluginData(self::PLUGIN_SLUG, self::PLUGIN_FILE, self::PLUGIN_TITLE);
		$deactivationTracker = new Tracker(
			$plugin_data,
			new Scripts($plugin_data, new DefaultReasonsFactory()),
			new Thickbox($plugin_data, new DefaultReasonsFactory()),
			new AjaxDeactivationDataHandler($plugin_data, $sender )
		);

		\WP_Mock::expectActionAdded( 'admin_print_footer_scripts-' . Tracker::HOOK_SUFFIX, [ $deactivationTracker,
			'printDeactivationScripts'
		]);
		\WP_Mock::expectActionAdded( 'admin_footer-' . Tracker::HOOK_SUFFIX, [ $deactivationTracker,
			'printDeactivationThickbox'
		]);

		$deactivationTracker->hooks();

		$this->assertTrue(true);
	}

	/**
	 * Tests printDeactivationScripts
	 * Tests if there is script in output.
	 */
	public function testPrintDeactivationScripts() {
		$plugin_data = new PluginData(self::PLUGIN_SLUG, self::PLUGIN_FILE, self::PLUGIN_TITLE);
		$tracker = TrackerFactory::createDefaultTrackerFromPluginData($plugin_data);
		$this->expectOutputRegex('/script/i');
		$tracker->printDeactivationScripts();
		$this->assertTrue(true);
	}

	/**
	 * Tests printDeactivationThickbox
	 * Tests if there is div in output.
	 */
	public function testPrintDeactivationThickbox() {
		$plugin_data = new PluginData(self::PLUGIN_SLUG, self::PLUGIN_FILE, self::PLUGIN_TITLE);
		$tracker = TrackerFactory::createDefaultTrackerFromPluginData($plugin_data);
		$this->expectOutputRegex('/div/i');
		$tracker->printDeactivationThickbox();
		$this->assertTrue(true);
	}

}
