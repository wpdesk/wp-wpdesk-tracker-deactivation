<?php

use WPDesk\Tracker\Deactivation\PluginData;

class TestPluginData extends \PHPUnit\Framework\TestCase {

	const PLUGIN_SLUG = 'slug';
	const PLUGIN_FILE = 'file';
	const PLUGIN_TITLE = 'title';

	private function createPluginData() {
		return new PluginData(self::PLUGIN_SLUG, self::PLUGIN_FILE, self::PLUGIN_TITLE);
	}

	public function testSetPluginFile() {
		$plugin_file = self::PLUGIN_FILE . '1';
		$pluginData = $this->createPluginData();
		$pluginData->setPluginFile($plugin_file);
		$this->assertEquals(self::PLUGIN_FILE . '1', $pluginData->getPluginFile());
	}

	public function testSetPluginSlug() {
		$plugin_slug = self::PLUGIN_SLUG . '1';
		$pluginData = $this->createPluginData();
		$pluginData->setPluginSlug($plugin_slug);
		$this->assertEquals($plugin_slug, $pluginData->getPluginSlug());
	}

	public function testSetPluginTitle() {
		$plugin_title = self::PLUGIN_TITLE . '1';
		$pluginData = $this->createPluginData();
		$pluginData->setPluginTitle($plugin_title);
		$this->assertEquals($plugin_title, $pluginData->getPluginTitle());
	}

	public function testGetPluginFile() {
		$pluginData = $this->createPluginData();
		$this->assertEquals(self::PLUGIN_FILE, $pluginData->getPluginFile());
	}

	public function testGetPluginTitle() {
		$pluginData = $this->createPluginData();
		$this->assertEquals(self::PLUGIN_TITLE, $pluginData->getPluginTitle());
	}

	public function testGetPluginSlug() {
		$pluginData = $this->createPluginData();
		$this->assertEquals(self::PLUGIN_SLUG, $pluginData->getPluginSlug());
	}

}
