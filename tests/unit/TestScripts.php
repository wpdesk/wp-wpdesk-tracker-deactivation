<?php

use WPDesk\Tracker\Deactivation\DefaultReasonsFactory;
use WPDesk\Tracker\Deactivation\PluginData;
use WPDesk\Tracker\Deactivation\Scripts;

class TestScripts extends Content {

	protected $view = __DIR__ . '/../../src/WPDesk/Tracker/Deactivation/views/scripts.php';

	/**
	 * Test getContent.
	 */
	public function testGetContent() {
		$scripts = new Scripts(new PluginData(self::PLUGIN_SLUG, self::PLUGIN_FILE, self::PLUGIN_TITLE), new DefaultReasonsFactory() );
		$this->assertEquals($this->getContent(), $scripts->getContent());
	}

}
