<?php

use WP_Mock\Tools\TestCase;
use WPDesk\Tracker\Deactivation\AjaxDeactivationDataHandler;
use WPDesk\Tracker\Deactivation\PluginData;

class TestAjax extends TestCase {

	const PLUGIN_SLUG = 'slug';
	const PLUGIN_FILE = 'file';
	const PLUGIN_TITLE = 'title';

	public function setUp(): void {
		\WP_Mock::setUp();
	}

	public function tearDown(): void {
		\WP_Mock::tearDown();
	}

	public function testHandleAjaxRequest() {
		\WP_Mock::userFunction( 'check_ajax_referer', array(
			'return' => true,
		) );
		\WP_Mock::userFunction( 'current_user_can', array(
		    'return' => true,
		) );

		$_REQUEST['reason'] = 'other';
		$_REQUEST['additional_info'] = 'test';

		$sender = $this->createMock(\WPDesk_Tracker_Sender::class);

		$ajax = new AjaxDeactivationDataHandler(new PluginData(self::PLUGIN_SLUG, self::PLUGIN_FILE, self::PLUGIN_TITLE), $sender);
		$this->expectOutputString('');
		$ajax->handleAjaxRequest();
	}
}
